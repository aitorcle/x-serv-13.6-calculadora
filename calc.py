def suma(a, b) -> float:
    return a + b


def resta(a, b) -> float:
    return a - b


print('El resultado de sumar 1 y 2 es: ', suma(1, 2))

print('El resultado de restar 6-5 es: ', resta(6,5))
print('El resultado de restar 7-8 es: ', resta(7,8))

